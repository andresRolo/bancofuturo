package com.banco.futuro.controller.impl;

import com.banco.futuro.controller.RiskController;
import com.banco.futuro.service.RiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author arueda
 *
 */
@RestController
public class RiskControllerImpl implements RiskController{

    @Autowired
	private RiskService riskService;

	public String[][] getMatrix(String[] impact, String[] urgency) throws Exception{
		return this.riskService.getMatrix(impact, urgency);
	}
}
