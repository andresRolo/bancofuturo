package com.banco.futuro.controller.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author arueda
 *
 */
@Entity
@Table(name="risk")
public class Risk implements Serializable{

	private static final long serialVersionUID = -5832898146577302701L;

	@Id
	private Integer id;
	private String matrix;
	private boolean highRisk;

	public Risk() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMatrix() {
		return matrix;
	}

	public void setMatrix(String matrix) {
		this.matrix = matrix;
	}

	public boolean isHighRisk() {
		return highRisk;
	}

	public void setHighRisk(boolean highRisk) {
		this.highRisk = highRisk;
	}
}
