package com.banco.futuro.controller;

import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author arueda
 *
 */
@ResponseBody
@RequestMapping(RiskController.RISK)
@Api(value = RiskController.VALUE,
		tags = {RiskController.VERSION})
public interface RiskController {

    String VERSION = "/1.0";
    String RISK = "/risk";
    String VALUE = "Risk Service";

	@PostMapping("/getMatrix")
	@ApiOperation(
			value = "get the matrix depending of impact and urgency"
    )
	@ApiResponses(
			value = {
					@ApiResponse(code = 200, message = "OK", response = String[][].class)
			}
	)
	String[][] getMatrix(@ApiParam(value = "impact", required = true) @RequestParam String[] impact,
					 @ApiParam(value = "urgency", required = true) @RequestParam String[] urgency) throws Exception;

}
