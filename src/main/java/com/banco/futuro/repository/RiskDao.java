package com.banco.futuro.repository;

import com.banco.futuro.controller.entity.Risk;
import com.meli.xmen.entity.Mutant;
import org.springframework.data.repository.CrudRepository;

/**
 * @author arueda
 *
 */
public interface RiskDao extends CrudRepository<Risk, Integer> {

}
