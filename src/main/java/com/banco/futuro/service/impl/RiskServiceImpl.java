package com.banco.futuro.service.impl;

import com.banco.futuro.controller.entity.Risk;
import com.banco.futuro.repository.RiskDao;
import com.banco.futuro.service.RiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * @author arueda
 */
@Service
public class RiskServiceImpl implements RiskService {

    @Autowired
    private RiskDao riskDao;

    private static final int impactSize = 3;
    private static final int urgencySize = 4;
    private static final int highRiskFrom = 90;

    /**
     * main matrix task
     * @param impact
     * @param urgency
     * @return
     * @throws Exception
     */
    public String[][] getMatrix(String[] impact, String[] urgency) throws Exception {
        validateMatrix(impact, urgency);
        String[][] matrix = generateMatrix(impact, urgency);
        boolean isHighRisk = isHighRisk(matrix);
        saveMatrix(matrix, isHighRisk);
        return matrix;
    }

    /**
     * validate request params
     * @param impact
     * @param urgency
     * @throws Exception
     */
    private void validateMatrix(String[] impact, String[] urgency) throws Exception {
        if (impact == null || impact.length < impactSize) {
            throw new Exception("Impact is not valid");
        }
        if (urgency == null || urgency.length < urgencySize) {
            throw new Exception("Urgency is not valid");
        }
    }

    /**
     * generate full matrix from impact and urgency calculating with external risk coefficient (random)
     * @param impact
     * @param urgency
     * @return
     */
    private String[][] generateMatrix(String[] impact, String[] urgency) {
        String matrix[][] = new String[impact.length][urgency.length];
        for (int j = 0; j < impact.length; j++) {
            String currentLine = urgency[j];
            for (int i = 0; i < currentLine.length(); i++) {
                int value = Character.getNumericValue(currentLine.charAt(i)) + this.generateRandomNumber();
                matrix[j][i] = String.valueOf(value);
            }
        }
        return matrix;
    }

    /**
     * generate random number to add the impact / urgency value
     * @return
     */
    private int generateRandomNumber() {
        Random r = new Random();
        int low = 1;
        int high = 10;
        int result = r.nextInt(high - low) + low;
        return result;
    }

    /**
     * Check if full matrix is high risk calculating if is above highRiskFrom value
     * @param matrix
     * @return
     */
    private boolean isHighRisk(String[][] matrix) {
        int totalMatrixRisk = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                totalMatrixRisk += Integer.parseInt(matrix[i][j]);
            }
        }
        if(totalMatrixRisk >= highRiskFrom){
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    /**
     * Save matrix in database
     * @param matrix
     * @param isHighRisk
     */
    private void saveMatrix(String[][] matrix, boolean isHighRisk) {
        Risk risk = new Risk();
        risk.setMatrix(matrix.toString());
        risk.setHighRisk(isHighRisk);
        riskDao.save(risk);
    }
}
