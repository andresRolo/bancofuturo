package com.banco.futuro.service;

/**
 * @author arueda
 */
public interface RiskService {

    String[][] getMatrix(String[] impact, String[] urgency) throws Exception;

}
