package com.banco.futuro;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

import java.io.IOException;

@SpringBootApplication
@ComponentScan("com.banco.futuro")
public class BancoDelFuturoApplication extends SpringBootServletInitializer implements CommandLineRunner {

	protected final Log logger = LogFactory.getLog(getClass());

	public static void main(String[] args) throws IOException {
		SpringApplication.run(BancoDelFuturoApplication.class, args);
	}

	public void run(String... args) throws Exception {
		logger.info("XmenApplication Started !!");
	}

}
